Please provide the input number to first param variable and second variabe for page result

Page 1 result:

Prev button should be diabled and next button value should be enable based on the "param" value i.e if param value is greater than list of results then enable.

Page 2 results:

next button value should be set to 2 for "page" variable



Request:
===========
http://<<ipaddress>>:<<portnumber>>/projectcontext/path/path/param/page

example:


http://localhost:8080/fizz-buzz/basic/fizzbuzz/50/2



Response:
===========

buzz
fizz
22
23
fizz
buzz
26
fizz
28
29
fizz buzz
31
32
fizz
34
buzz
fizz
37
38
fizz
buzz