package com.capgemini.fizz;

import java.util.Calendar;
import java.util.Date;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

/*
 * 
 */
@Path("/basic")
public class FizzBuzzService {

	@GET
	@Path("/fizzbuzz/{param}/{page}")
	public Response fizzBuzz(@PathParam("param") String str, @PathParam("page") int page) {
	System.out.println("Class="+getClass()+" | method= fizzBuzz start");
		
		String responeStr = "";
		System.out.println("Please enter positive integer. \n");
		
		int inputInt = -1;
		String s  = str;
		//checking for null or empty value
		if (null == s || s.isEmpty() ) {
			
			responeStr = "Provided input is NULl or empty, Please pass valid intput";
			return Response.status(500).entity(responeStr).build();
			
		}
		
		System.out.println("You have entered value is :"+s);
		try {
			
			
			inputInt = Integer.parseInt(s);
			int check =  Integer.signum(inputInt);
			if(check == -1) {
				
				responeStr = "Please enter only positive integer, You have entered is not a postive integer, entered is:"+s;
				return Response.status(500).entity(responeStr).build();
				
			}else if(check == 0) {
				
				responeStr = "Please enter only positive integer, You have entered is not a postive integer, entered is:"+s;
				return Response.status(500).entity(responeStr).build();
				
			}else if(check == 1) {
				
				//if 
				if(inputInt < 1 || inputInt > 1000 ) {
					responeStr = "Provided input is out of boundry, Please provide integer value between 1 and 1000 only";
					return Response.status(500).entity(responeStr).build();
				}
				
				//checking today is wednesday
				Calendar cal = Calendar.getInstance();
				cal.setTime(new Date());
				boolean wednesday = cal.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY;
				
				//page nation logic
				int first = 0;
				if(page == 1) {
					first = page;
				}else if(page > 1)  {
					first = page*20-20;
				}
				int last = (page*20);
				if(last > inputInt ) {
					last = inputInt;
				}
				for(int i = first ; i <= last ; i ++) {
					
					int divide3 =  i % 3;
					int divide5 = i % 5;
					
					if( divide3 == 0 && divide5 == 0 ) {
						if(wednesday) {
							System.out.println("wizz wuzz");
							responeStr = responeStr + "wizz wuzz"+ "\n";
						}else {
							System.out.println("fizz buzz");
							responeStr = responeStr + "fizz buzz"+ "\n";
						}
						continue;
					}
					
					if( divide3 == 0 ) {
						if(wednesday) {
							System.out.println("wizz");
							responeStr = responeStr + "wizz"+ "\n";
						}else {
							System.out.println("fizz");
							responeStr = responeStr + "fizz"+ "\n";
						}
						continue;
					}
					if( divide5 == 0 ) {
						if(wednesday) {
							System.out.println("wuzz");
							responeStr = responeStr + "wuzz"+ "\n";
						}else {
							System.out.println("buzz");
							responeStr = responeStr + "buzz"+ "\n";
						}
						continue;
					}
					System.out.println(i);
					responeStr = responeStr + i + "\n";
						
						
					
				}
				
			}
		}catch (java.util.InputMismatchException e) {
			
			//System.out.println("Please enter only positive integer, You have entered is not a postive integer, entered is:"+s);
			responeStr = "Please enter only positive integer, You have entered is not a postive integer, entered is:"+s;
			return Response.status(500).entity(responeStr).build();
		}catch (java.lang.NumberFormatException e) {
			//System.out.println("Please enter only positive integer, You have entered is not a postive integer, entered is:"+s);
			responeStr = "Please enter only positive integer, You have entered is not a postive integer, entered is:"+s;
			return Response.status(500).entity(responeStr).build();
		}catch (Exception e) {
			
			//System.out.println("Error occurred - Please validate the input and please enter the positive integer.");
			responeStr = "Runtime Exceptin occurred - Please enter only positive integer, You have entered is not a postive integer, entered is:"+s;
			return Response.status(500).entity(responeStr).build();
		}
		
		finally {
			
		}
		
		return Response.status(200).entity(responeStr).build();
					
	}

}
