import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class FizzBuzzTest {
	
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Please enter positive integer. \n");
		
		int inputInt = -1;
		String s  = scan.nextLine();
		String responseStr = "";
		System.out.println("You have entered value is :"+s);
		try {
			
			inputInt = Integer.parseInt(s);
			int check =  Integer.signum(inputInt);
			if(check == -1) {
				System.out.println("Please enter only positive integer, You have entered is not a postive integer, entered is:"+s);
				return ;
			}else if(check == 0) {
				System.out.println("Please enter only positive integer, You have entered is not a postive integer, entered is:"+s);
				return;
			}else if(check == 1) {
				
					//page natin value is hard coded here we can change this value.
					int page = 2;
					
					if(inputInt < 1 || inputInt > 1000 ) {
						System.out.println("Provided input is out of boundry, Please provide integer value between 1 and 1000 only");
						return;
					}
					
					
					Calendar cal = Calendar.getInstance();
					cal.setTime(new Date());
					boolean wednesday = cal.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY;
					
					int first = 0;
					if(page == 1) {
						first = page;
					}else if(page > 1)  {
						first = page*20-20;
					}
					int last = (page*20);
					if(last > inputInt ) {
						last = inputInt;
					}
					//for(int i = page*20 ; i <= (page*20)+20 ; i ++) {
					for(int i = first ; i <= last ; i ++) {
						
						int divide3 =  i % 3;
						int divide5 = i % 5;
						
						if( divide3 == 0 && divide5 == 0 ) {
							if(wednesday) {
								System.out.println("wizz wuzz");
								responseStr = responseStr + "wizz wuzz"+ "\n";
							}else {
								System.out.println("fizz buzz");
								responseStr = responseStr + "fizz buzz"+ "\n";
							}
							continue;
						}
						
						if( divide3 == 0 ) {
							if(wednesday) {
								System.out.println("wizz");
								responseStr = responseStr + "wizz"+ "\n";
							}else {
								System.out.println("fizz");
								responseStr = responseStr + "fizz"+ "\n";
							}
							continue;
						}
						if( divide5 == 0 ) {
							if(wednesday) {
								System.out.println("wuzz");
								responseStr = responseStr + "wuzz"+ "\n";
							}else {
								System.out.println("buzz");
								responseStr = responseStr + "buzz"+ "\n";
							}
							continue;
						}
						System.out.println(i);
						responseStr = responseStr + i + "\n";
							
						
					}
						
					
				}
			System.out.println("final str :"+responseStr);
	
		}catch (java.util.InputMismatchException e) {
			
			System.out.println("Please enter only positive integer, You have entered is not a postive integer, entered is:"+s);
			return ;
		}catch (java.lang.NumberFormatException e) {
			System.out.println("Please enter only positive integer, You have entered is not a postive integer, entered is:"+s);
			return ;
		}catch (Exception e) {
			
			System.out.println("Error occurred - Please validate the input and please enter the positive integer.");
			return ;
		}
		
		finally {
			scan.close();
		}
		
	}

}
